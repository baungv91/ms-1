package com.b.project.authorizationserver;

import com.b.project.authorizationserver.model.User;
import com.b.project.authorizationserver.repo.UserDetailRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

@SpringBootApplication
@EnableAuthorizationServer
public class AuthorizationServerApplication {

    public static void main(String[] args) {

        ApplicationContext context = SpringApplication.run(AuthorizationServerApplication.class, args);
//        UserDetailRepository repo = context.getBean(UserDetailRepository.class);
//        repo.save(User.builder()
//                .username("demo1")
//                .password(context.getBean(PasswordEncoder.class).encode("1234"))
//                .email("demo@gmail.com")
//                .enabled(true)
//                .build());
    }

}
