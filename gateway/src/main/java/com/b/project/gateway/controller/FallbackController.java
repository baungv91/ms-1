package com.b.project.gateway.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FallbackController {
    @GetMapping("/service1")
    public String service1Fallback(){
        return "not found!";
    }
}
