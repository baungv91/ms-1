package com.b.project.gateway.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RouteConfig {

    @Bean
    public RouteLocator route(RouteLocatorBuilder builder) {
        return builder
                .routes()
                .route(r -> r
                                .path("/service1/**")
                                .filters(f -> f.addResponseHeader("Hello", "World")
                                .hystrix(h -> h.setFallbackUri("forward:/service1")))
                        .uri("lb://FIRST-SERVICEV")
//                                .uri("http://localhost:9001")
                )
                .build();
    }
}
