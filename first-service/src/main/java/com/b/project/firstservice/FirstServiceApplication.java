package com.b.project.firstservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableDiscoveryClient
@RestController
@RequestMapping("/service1")
public class FirstServiceApplication {

    @Value("${server.port}")
    private String port;

    @GetMapping("demo1")
    public String demo1(){
        return port;
    }

    public static void main(String[] args) {
        SpringApplication.run(FirstServiceApplication.class, args);
    }

}
